export function rowspan(skills) {
    return `row-span-${Math.floor(Object.keys(skills).length / 2 ?? 1)}`
}
