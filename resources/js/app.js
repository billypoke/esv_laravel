import { createInertiaApp } from "@inertiajs/vue3"
import { resolvePageComponent } from "laravel-vite-plugin/inertia-helpers"
import { createApp, h } from "vue"
import VueTippy from "vue-tippy"
import { ZiggyVue } from "ziggy-js"

import "./fontawesome.js"

const appName = import.meta.env.VITE_APP_NAME || "Laravel"

createInertiaApp({
    title: (title) => (title.length === 0 ? appName : `${title} - ${appName}`),
    resolve: (name) =>
        resolvePageComponent(
            `./Pages/${name}.vue`,
            import.meta.glob("./Pages/**/*.vue"),
        ),
    setup: ({ el, App, props, plugin }) =>
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(VueTippy, {
                defaultProps: {
                    allowHTML: true,
                    maxWidth: "none",
                    placement: "auto",
                },
            })
            .use(ZiggyVue)
            .mount(el),
    progress: {
        color: "#2DD4BF",
        delay: 0,
    },
})
