<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#060606">
    <meta name="description"
          content="{{ config('services.eveonline.user_agent') }}">

    <link rel="shortcut icon" type="image/png"
          href="https://images.evetech.net/types/11578/icon?size=64">

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @routes
    @vite(['resources/js/app.js', 'resources/css/app.css', "resources/js/Pages/{$page['component']}.vue"])
    @inertiaHead
</head>
<body class="bg-zinc-900 text-zinc-200">
@inertia
</body>
</html>
