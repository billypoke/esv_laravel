<?php

declare(strict_types=1);

use App\Http\Controllers\Auth\EveSsoController;
use Illuminate\Support\Facades\Route;

Route::get(
    'login',
    [EveSsoController::class, 'redirectToProvider']
)->name('login');

Route::get(
    'eve_login_callback',
    [EveSsoController::class, 'handleProviderCallback']
)->name('handle_callback');

Route::middleware('auth')->post('logout', function() {
    Auth::guard('web')->logout();

    Session::invalidate();
    Session::regenerateToken();

    return to_route('landing');
})->name('logout');
