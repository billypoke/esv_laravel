<?php

declare(strict_types=1);

use App\Http\Controllers\Esi\DestroyCharacterController;
use App\Http\Controllers\Esi\LandingController;
use App\Http\Controllers\Esi\IndexCharactersController;
use App\Http\Controllers\Esi\ShowCharacterController;
use Illuminate\Support\Facades\Route;

Route::middleware('web')
    ->get('/', LandingController::class)
    ->name('landing');

Route::middleware('auth:web')->group(function (): void {
    Route::prefix('characters')
        ->name('characters.')
        ->scopeBindings()
        ->group(function (): void {
            Route::get('', IndexCharactersController::class)->name('index');
            Route::get(
                '{character:character_id}',
                ShowCharacterController::class,
            )->name('show');
            Route::delete(
                '{character:character_id}',
                DestroyCharacterController::class,
            )->name('destroy');
        });
});

require __DIR__.'/auth.php';
