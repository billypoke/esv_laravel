<?php

declare(strict_types=1);

use App\Jobs\CacheEsiGroups;
use App\Jobs\CacheSavedCharactersSkillQueues;
use App\Jobs\CacheSavedCharactersSkills;
use App\Jobs\CacheStatus;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();

Schedule::job(CacheEsiGroups::class)->dailyAt('11:10');

Schedule::job(CacheStatus::class)->everyThirtySeconds();

Schedule::job(CacheSavedCharactersSkills::class)->everyTwoMinutes();

Schedule::job(CacheSavedCharactersSkillQueues::class)->everyTwoMinutes();
