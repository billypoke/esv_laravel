<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Esi\EsiClient;
use App\View\Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Inertia\Middleware;
use Override;

class HandleInertiaRequests extends Middleware
{
    public function __construct(
        private readonly EsiClient $esiClient,
    ) {}

    #[Override]
    public function share(Request $request): array
    {
        return [
            ...parent::share($request),
            'auth' => [
                'user' => $request->user(),
            ],
            'appName' => config('app.name'),
            'flash' => $this->getFlash(),
            'hcaptcha_sitekey' => config('services.hcaptcha.sitekey'),
            'status' => $this->esiClient->query('status'),
        ];
    }

    /**
     * @return array<string>|null
     */
    private function getFlash(): ?array
    {
        foreach (Flash::cases() as $type) {
            if (Session::has($type->value)) {
                return [
                    'message' => (string) Session::get($type->value),
                    'type' => $type->value,
                ];
            }
        }

        return null;
    }
}
