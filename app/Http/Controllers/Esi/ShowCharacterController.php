<?php

declare(strict_types=1);

namespace App\Http\Controllers\Esi;

use App\Esi\User;
use App\Models\Character;
use App\Models\Token;
use App\View\Flash;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;
use Inertia\Response;

class ShowCharacterController extends CharacterController
{
    /**
     * @throws AuthorizationException
     */
    public function __invoke(Character $character): RedirectResponse|Response
    {
        Gate::allowIf(auth()->user()?->characters->contains($character));

        $userStart = microtime(true);
        try {
            /** @var Token $token */
            $token = $character->tokens()->latest()->firstOrFail();

            /** @var User $storedCharacter */
            $storedCharacter = $this->retrieveStoredCharacter($token);
        } catch (RequestException $e) {
            if ($e->getCode() === 400) {
                return to_route('eve_login');
            }

            return to_route('characters.index')->with(
                Flash::Danger->value,
                'Could not retrieve character data'
            );
        }
        $userEnd = microtime(true);

        try {
            $skills = $this->parser->parse(
                $token->access_token,
                (int) $storedCharacter->character_id,
                $userEnd - $userStart,
            );
        } catch (Exception $e) {
            report($e);

            return to_route('landing')->with(
                Flash::Danger->value,
                'An internal error has occurred. Please try again later.'
            );
        }

        return Inertia::render('Characters/Show', [
            'character' => $storedCharacter,
            'summary' => $skills->summary,
            'stats' => $skills->stats,
            'skills' => $skills->skills,
            'sections' => $skills->sections,
        ]);
    }
}
