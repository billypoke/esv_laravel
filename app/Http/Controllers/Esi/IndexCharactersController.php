<?php

declare(strict_types=1);

namespace App\Http\Controllers\Esi;

use App\Esi\Summary;
use App\Esi\User;
use App\Models\Character;
use App\Models\Token;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\RedirectResponse;
use Inertia\Inertia;
use Inertia\Response;

class IndexCharactersController extends CharacterController
{
    /**
     * @throws RequestException
     */
    public function __invoke(): RedirectResponse|Response
    {
        $userStart = microtime(true);

        try {
            $characters = auth()->user()
                ?->characters
                ->map(fn (Character $character): array => $this->getCharacterData(
                    $character->tokens()->latest()->first(),
                    $userStart
                ));
        } catch (RequestException $e) {
            if ($e->getCode() === 400) {
                return to_route('eve_login');
            }

            throw $e;
        }

        return Inertia::render('Characters/Index', ['characters' => $characters]);
    }

    /**
     * @return array{'character': User, 'summary': Summary}
     *
     * @throws RequestException
     */
    protected function getCharacterData(
        Token $token,
        float $userStart
    ): array {
        /** @var User $character */
        $character = $this->retrieveStoredCharacter($token);

        return [
            'character' => $character,
            'summary' => $this->parser
                ->parse(
                    $character->token,
                    (int) $character->character_id,
                    microtime(true) - $userStart,
                )
                ->summary,
        ];
    }
}
