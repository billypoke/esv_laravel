<?php

declare(strict_types=1);

namespace App\Http\Controllers\Esi;

use App\Http\Controllers\Controller;
use App\Models\Character;
use App\View\Flash;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;

class DestroyCharacterController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function __invoke(Character $character): RedirectResponse
    {
        Gate::allowIf(auth()->user()?->characters->contains($character));

        $character->delete();

        return to_route('characters.index')->with(Flash::Success->value, 'Character Removed');
    }
}
