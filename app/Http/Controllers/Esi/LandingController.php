<?php

declare(strict_types=1);

namespace App\Http\Controllers\Esi;

use App\Esi\SkillParser;
use App\Http\Controllers\Controller;
use App\Traits\RetrievesCharacters;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Traits\Conditionable;
use Inertia\Inertia;
use Inertia\Response;

class LandingController extends Controller
{
    public function __invoke(): Response
    {
        return Inertia::render('Landing');
    }
}
