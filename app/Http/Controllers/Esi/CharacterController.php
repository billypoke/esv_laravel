<?php

declare(strict_types=1);

namespace App\Http\Controllers\Esi;

use App\Esi\SkillParser;
use App\Http\Controllers\Controller;
use App\Traits\RetrievesCharacters;

class CharacterController extends Controller
{
    use RetrievesCharacters;

    public function __construct(
        protected readonly SkillParser $parser,
    ) {}
}
