<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Esi\User as EsiUser;
use App\Http\Controllers\Controller;
use App\Models\Character;
use App\Models\Token;
use App\Models\User;
use App\View\Flash;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Laravel\Socialite\Facades\Socialite;
use SocialiteProviders\Eveonline\Provider;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class EveSsoController extends Controller
{
    public function redirectToProvider(): Response
    {
        /** @var Provider $driver */
        $driver = Socialite::driver('eveonline');

        Assert::isInstanceOf($driver, Provider::class);

        return Inertia::location(
            $driver->scopes(config('services.eveonline.scope'))
                ->redirect()
                ->getTargetUrl()
        );
    }

    public function handleProviderCallback(): RedirectResponse
    {
        JWT::$leeway = 10;

        /** @var EsiUser $esiUser */
        $esiUser = Socialite::driver('eveonline')->user();

        if (Auth::check()) {
            $appUser = Auth::user();
        } else {
            // Attempt to find the user based on character owner hash
            $appUser = User::query()
                ->whereRelation(
                    'characters',
                    'character_owner_hash',
                    $esiUser->character_owner_hash,
                )
                ->firstOrCreate();
        }

        // We need to create or update this character,
        // reassigning it to the current owner's user if needed
        $character = Character::query()->updateOrCreate([
            'character_id' => $esiUser->character_id,
        ], [
            'character_owner_hash' => $esiUser->character_owner_hash,
            'user_id' => $appUser->id,
            'character_name' => $esiUser->character_name,
        ]);

        Token::query()->updateOrCreate([
            'character_id' => $esiUser->character_id,
        ], [
            'access_token' => $esiUser->token,
            'refresh_token' => $esiUser->refreshToken,
            'expires_in' => $esiUser->expiresIn,
        ]);

        Auth::login($appUser);

        return to_route('characters.show', $character->character_id)
            ->with(Flash::Success->value, 'Successfully Retrieved Character!');
    }
}
