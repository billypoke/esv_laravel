<?php

declare(strict_types=1);

namespace App\Parsing\Contracts;

use App\Esi\EsiResponse;
use App\Esi\SkillTypeInformation;
use Spatie\LaravelData\Data;

interface DataProcessor
{
    public function parse(SkillTypeInformation $typeInformation, EsiResponse $skillsAndQueue): Data;
}
