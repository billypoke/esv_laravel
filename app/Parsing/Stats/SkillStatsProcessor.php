<?php

declare(strict_types=1);

namespace App\Parsing\Stats;

use App\Esi\Entities\Skill;
use App\Esi\EsiResponse;
use App\Esi\SkillTypeInformation;
use App\Parsing\Contracts\DataProcessor;
use Illuminate\Support\Collection;

class SkillStatsProcessor implements DataProcessor
{
    public function parse(
        SkillTypeInformation $typeInformation,
        EsiResponse $skillsAndQueue
    ): Stats {
        return new Stats($typeInformation->skillGroups
            ->map(fn (
                Collection $skillIds,
                string $groupName
            ): StatsGroup => $this->buildStatsGroup(
                $groupName,
                $skillIds,
                $skillsAndQueue->skillsById->filter(
                    fn (Skill $skill) => $skillIds->contains($skill->skillId)
                ),
                $typeInformation
            ))
            ->sortByDesc('spInGroup')
            ->values());
    }

    /**
     * @param  Collection<int, int>  $skillIds
     * @param  Collection<int, Skill>  $trainedSkillsInGroup
     */
    public function buildStatsGroup(
        string $groupName,
        Collection $skillIds,
        Collection $trainedSkillsInGroup,
        SkillTypeInformation $typeInformation
    ): StatsGroup {
        return new StatsGroup(
            groupName: $groupName,
            skillCount: $skillIds->count(),
            trainedCount: $trainedSkillsInGroup->count(),
            spInGroup: (int) $trainedSkillsInGroup->sum('skillpointsInSkill'),
            groupSkills: $trainedSkillsInGroup
                ->map(fn (Skill $skill): SkillWithName => new SkillWithName(
                    activeSkillLevel: $skill->activeSkillLevel,
                    skillId: $skill->skillId,
                    skillpointsInSkill: $skill->skillpointsInSkill,
                    trainedSkillLevel: $skill->trainedSkillLevel,
                    skillName: $typeInformation->skillNames->get($skill->skillId),
                ))
                ->sortBy('skillName')
                ->values(),
        );
    }
}
