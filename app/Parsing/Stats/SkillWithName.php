<?php

declare(strict_types=1);

namespace App\Parsing\Stats;

use Spatie\LaravelData\Data;

class SkillWithName extends Data
{
    public function __construct(
        public readonly int $activeSkillLevel,
        public readonly int $skillId,
        public readonly int $skillpointsInSkill,
        public readonly int $trainedSkillLevel,
        public readonly string $skillName,
    ) {}
}
