<?php

declare(strict_types=1);

namespace App\Parsing\Stats;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class Stats extends Data
{
    /**
     * @param  Collection<int, StatsGroup>  $statsGroups
     */
    public function __construct(
        public readonly Collection $statsGroups,
    ) {}
}
