<?php

declare(strict_types=1);

namespace App\Parsing\Stats;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class StatsGroup extends Data
{
    /**
     * @param  Collection<int, SkillWithName>  $groupSkills
     */
    public function __construct(
        public readonly string $groupName,
        public readonly int $skillCount,
        public readonly int $trainedCount,
        public readonly int $spInGroup,
        public readonly Collection $groupSkills,
    ) {}
}
