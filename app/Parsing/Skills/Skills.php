<?php

declare(strict_types=1);

namespace App\Parsing\Skills;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class Skills extends Data
{
    /**
     * @param  Collection<string, SkillGroup>  $groupedSkills
     */
    public function __construct(
        public Collection $groupedSkills,
    ) {}
}
