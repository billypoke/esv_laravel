<?php

declare(strict_types=1);

namespace App\Parsing\Skills;

use Spatie\LaravelData\Data;

class Skill extends Data
{
    public function __construct(
        public readonly string $skillName,
        public readonly ?int $skillLevel,
    ) {}
}
