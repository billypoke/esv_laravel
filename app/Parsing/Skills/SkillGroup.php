<?php

declare(strict_types=1);

namespace App\Parsing\Skills;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class SkillGroup extends Data
{
    /**
     * @param  Collection<int, Skill>  $skills
     */
    public function __construct(
        public readonly string $groupName,
        public readonly Collection $skills,
    ) {}
}
