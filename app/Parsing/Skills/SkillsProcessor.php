<?php

declare(strict_types=1);

namespace App\Parsing\Skills;

use App\Esi\EsiResponse;
use App\Esi\SkillTypeInformation;
use App\Parsing\Contracts\DataProcessor;
use Illuminate\Support\Collection;

class SkillsProcessor implements DataProcessor
{
    public function parse(
        SkillTypeInformation $typeInformation,
        EsiResponse $skillsAndQueue
    ): Skills {
        return new Skills($typeInformation->skillGroups
            ->map(fn (
                Collection $skillIds,
                string $groupName
            ): SkillGroup => new SkillGroup(
                groupName: $groupName,
                skills: $skillIds
                    ->map(fn (int $skillId): Skill => new Skill(
                        skillName: $typeInformation->skillNames->get($skillId),
                        skillLevel: $skillsAndQueue
                            ->skillsById
                            ->get($skillId)
                            ?->activeSkillLevel,
                    ))
            )));
    }
}
