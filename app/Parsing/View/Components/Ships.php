<?php

declare(strict_types=1);

namespace App\Parsing\View\Components;

use App\Parsing\Skills\Skill;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Ships extends Component
{
    /**
     * @var array<string, string[]>
     */
    private array $advancedShips = [
        'Battleship' => [
            'Marauders',
            'Black Ops',
        ],
        'Battlecruiser' => [
            'Command Ships',
        ],
        'Cruiser' => [
            'Recon Ships',
            'Heavy Assault Cruisers',
            'Heavy Interdiction Cruisers',
            'Logistics Cruisers',
        ],
        'Destroyer' => [
            'Interdictors',
            'Command Destroyers',
            // Tactical destroyer filled by constructor due to faction variants
        ],
        'Frigate' => [
            'Interceptors',
            'Assault Frigates',
            'Covert Ops',
            'Electronic Attack Ships',
            'Logistics Frigates',
        ],
    ];

    /**
     * @var string[]
     */
    private array $t3Cruisers = [
        'Amarr' => 'Legion',
        'Caldari' => 'Tengu',
        'Gallente' => 'Proteus',
        'Minmatar' => 'Loki',
    ];

    /**
     * @var string[]
     */
    private array $t3Subsystems = [
        'Offensive',
        'Defensive',
        'Propulsion',
        'Core',
    ];

    /**
     * @var array<string, string[]>
     */
    private array $capitalSupportSkills = [
        'Piloting' => [
            'Advanced Spaceship Command',
            'Capital Ships',
        ],
        'Jump' => [
            'Cynosural Field Theory',
            'Jump Drive Calibration',
            'Jump Drive Operation',
            'Jump Fuel Conservation',
        ],
        'Carrier' => [
            'Fighters',
            'Fighter Hangar Management',
            'Light Fighters',
            'Support Fighters',
            'Heavy Fighters',
        ],
    ];

    public function parse(): Collection
    {
        // Populate the ships that have faction specific skills
        foreach ($this->empireFactions as $faction) {
            $this->advancedShips['Destroyer'][] = "$faction Tactical Destroyer";
        }

        return collect([
            'T1 Subcapital Ships' => $this->parseSubcaps(),
            'T2 Advanced Ships' => $this->parseAdvancedSubcaps(),
            'T3 Strategic Cruisers' => $this->parseT3s(),
            'Ship Support' => $this->parseMisc(),
            'Capital Ships' => $this->parseCapitals(),
            'Capital Support' => $this->parseCapitalSupport(),
        ]);
    }

    /**
     * Top level function to arrange the regular subcap skills into the correct format.
     *
     * @return Collection<string, Collection<int, Skill>>
     */
    private function parseSubcaps(): Collection
    {
        // $skills are in the format $skillName => $trainedLevel
        return $this->groupSkills
            ->skills
            ->filter($this->skillsForTypeFilterFn($this->subcapTypes))
            // $skillName is in the format "$faction $type",
            // Group into Frigate, Destroyer, etc.
            ->mapToGroups($this->groupMapFn())
            // Sort by faction
            ->map(
                fn (Collection $skills): Collection => $skills->sortBy($this->byFaction())->values()
            )
            // Sort the groups by the size order
            ->sortBy($this->sortGroupsFn($this->subcapTypes));
    }

    /**
     * Filter function for reducing the complete list of skills to a specified type.
     *
     * @param  array<string>  $types
     * @return Closure(Skill): bool
     */
    private function skillsForTypeFilterFn(array $types): callable
    {
        return fn (Skill $skill): bool => collect($this->allFactions)
            // Gives all possible combinations of faction and type
            ->crossJoin($types)
            // Combine each back to a single value
            /** @phpstan-ignore-next-line */
            ->mapSpread(fn (string $faction, string $type): string => $faction && $type
                ? sprintf('%s %s', $faction, $type)
                : ''
            )
            ->contains($skill->skillName);
    }

    /**
     * Helper function.
     *
     * $skill is in the format "$faction $type",
     *
     * @return Closure(Skill): array<string, Skill>
     */
    private function groupMapFn(): callable
    {
        return static fn (Skill $skill): array => [
            explode(' ', $skill->skillName)[1] => $skill,
        ];
    }

    /**
     * Helper function.
     *
     * $skill is in the format "$faction $type",
     *
     * @return Closure(Skill): string
     */
    private function byFaction(): callable
    {
        return fn (Skill $skill): int|string|false => array_search(
            explode(' ', $skill->skillName)[0],
            $this->allFactions,
            true,
        );
    }

    /**
     * Helper function.
     *
     * @param  array<string>  $types
     * @return Closure(string, string): int
     */
    private function sortGroupsFn(array $types): callable
    {
        return static fn (string $_, string $groupName): int|string|false => array_search(
            $groupName,
            $types,
            true,
        );
    }

    /**
     * Top level function to arrange the advanced subcap skills into the correct format.
     *
     * @return Collection<string, Collection<int, Skill>>
     */
    private function parseAdvancedSubcaps(): Collection
    {
        // $skills are in the format $skillName => $trainedLevel
        return collect($this->advancedShips)
            // We want all sub-arrays to be collections as well
            ->recursive()
            // Convert the skill names as values to keys with the trained level as the value
            ->map(fn (Collection $groupSkills) => $groupSkills
                ->mapWithKeys(
                    fn (string $skillName) => [
                        $skillName => $this->groupSkills->skills->firstWhere(
                            'skillName',
                            $skillName
                        ),
                    ]
                )
            )
            // Sort the groups by the size order
            ->sortBy($this->sortGroupsFn($this->subcapTypes))
            ->filter();
    }

    /**
     * Top level function to arrange the T3 skills into the correct format.
     *
     * @return Collection<string, Collection<string, Skill>>
     */
    private function parseT3s(): Collection
    {
        return collect($this->t3Cruisers)
            // Convert the skill names as values to keys with the trained level as the value
            ->flatMap($this->t3NamesToTrainedLevels())
            ->filter();
    }

    /**
     * Mapping function for parsing of T3.
     *
     * @return Closure(): Collection<string, Collection<string, Skill>>
     *
     * @see Ships::parseT3s()
     */
    private function t3NamesToTrainedLevels(): callable
    {
        return fn () => collect($this->empireFactions)
            ->mapWithKeys($this->t3MapValueFn());
    }

    /**
     * Gets the array of faction => T3 skills.
     *
     * @return Closure(string): array<string, Collection<string, Skill>>
     *
     * @see Ships::parseT3s()
     */
    private function t3MapValueFn(): callable
    {
        $allT3Skills = collect($this->empireFactions)
            ->crossJoin($this->t3Subsystems)
            /** @phpstan-ignore-next-line */
            ->mapSpread($this->getT3FactionSubsystem());

        return fn (string $empireFaction) => [
            $empireFaction => collect([
                $this->t3Cruisers[$empireFaction] => $this->groupSkills->skills->firstWhere('skillName',
                    sprintf('%s Strategic Cruiser', $empireFaction)),
            ])
                ->union(
                    $this->t3SkillsForFaction($allT3Skills, $empireFaction)
                        ->mapWithKeys(
                            fn (string $skillName) => [
                                $skillName => $this->allSkillGroups->get('Subsystems')
                                    ->skills
                                    ->firstWhere('skillName', $skillName),
                            ]
                        ),
                )
                ->filter(),
        ];
    }

    /**
     * Get the combined skill name for a given faction/subsystem pairing.
     *
     * @return Closure(string, string): string
     *
     * @see Ships::parseT3s()
     */
    private function getT3FactionSubsystem(): callable
    {
        return static fn (
            string $faction,
            string $system
        ): string => sprintf('%s %s Systems', $faction, $system);
    }

    /**
     * Map the T3 subsystem skills (Offensive, Defensive, etc.) for a given faction.
     *
     * @param  Collection<int, string>  $allT3Skills
     * @return Collection<int, string>
     *
     * @see Ships::parseT3s()
     */
    private function t3SkillsForFaction(
        Collection $allT3Skills,
        string $empireFaction
    ): Collection {
        return $allT3Skills->filter(fn (string $skill) => Str::contains($skill, $empireFaction));
    }

    /**
     * @return Collection<string, Collection<int, Skill>>
     */
    private function parseMisc(): Collection
    {
        $engSkills = $this->allSkillGroups->get('Engineering');

        return collect([
            'Fitting' => $engSkills->skills->filter(
                fn (Skill $skill): bool => in_array(
                    $skill->skillName,
                    [
                        'Weapon Upgrades',
                        'Advanced Weapon Upgrades',
                        'CPU Management',
                        'Power Grid Management',
                        'Energy Grid Upgrades',
                    ],
                    true,
                )),
            'Overheating' => $engSkills->skills->filter(
                fn (Skill $skill): bool => in_array(
                    $skill->skillName,
                    [
                        'Thermodynamics',
                        'Nanite Interfacing',
                        'Nanite Operation',
                    ],
                    true,
                )),
        ]);
    }

    /**
     * Top level function to arrange the Capital skills into the correct format.
     *
     * @return Collection<string, Collection<int, Skill>>
     */
    private function parseCapitals(): Collection
    {
        return $this->groupSkills
            ->skills
            ->filter($this->skillsForTypeFilterFn($this->capitalTypes))
            ->mapToGroups($this->groupMapFn())
            ->map(
                fn (Collection $groupSkills) => $groupSkills->sortBy($this->byFaction())
            )
            ->sortBy($this->sortGroupsFn($this->capitalTypes))
            ->filter();
    }

    /**
     * Parse capital support skills.
     *
     * @return Collection<string, Collection<int, Skill>>
     *
     * @see Ships::parseCapitals()
     */
    private function parseCapitalSupport(): Collection
    {
        return collect($this->capitalSupportSkills)
            ->recursive()
            ->map($this->capitalMapFn());
    }

    /**
     * Map Capital skills to their levels, grouped into the major types.
     *
     * @return Closure(Collection<string, ?int>, string): Collection<int, Skill>
     *
     * @see Ships::parseCapitals()
     */
    private function capitalMapFn(): callable
    {
        return fn (Collection $groupSkills, string $groupName) => $groupSkills
            ->when(
                $groupName === 'Piloting',
                $this->capitalSkillsInGroup('Spaceship Command')
            )
            ->when(
                $groupName === 'Jump',
                $this->capitalSkillsInGroup('Navigation')
            )
            ->when(
                $groupName === 'Carrier',
                $this->capitalSkillsInGroup('Drones')
            );
    }

    /**
     * This is the mapping function to get only the correct skills for Capital groups.
     *
     * @return Closure(Collection<int, Skill>): Collection<int, Skill>
     *
     * @see Ships::parseCapitals()
     */
    private function capitalSkillsInGroup(string $skillGroup): callable
    {
        return fn (Collection $skillNames) => $this->allSkillGroups
            ->get($skillGroup)
            ->skills
            ->filter(fn (Skill $skill) => $skillNames->contains($skill->skillName));
    }
}
