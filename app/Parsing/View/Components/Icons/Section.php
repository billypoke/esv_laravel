<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

use Spatie\LaravelData\Data;

abstract class Section extends Data
{
    /**
     * @param  list<SectionItem>  $section
     */
    public function __construct(
        public readonly array $section,
    ) {}
}
