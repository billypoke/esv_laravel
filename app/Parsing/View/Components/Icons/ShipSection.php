<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

class ShipSection extends Section
{
    public function __construct()
    {
        parent::__construct([
            new SectionItem(
                'T1 Subcapital Ships',
                [
                    'space-shuttle',
                    'angle-down',
                ],
                [
                    new SectionItem('Frigate', [
                        'chess-pawn',
                    ]),
                    new SectionItem('Destroyer', [
                        'chess-pawn',
                        'plus',
                    ]),
                    new SectionItem('Cruiser', [
                        'chess-knight',
                    ]),
                    new SectionItem('Battlecruiser', [
                        'chess-knight',
                        'plus',
                    ]),
                    new SectionItem('Battleship', [
                        'chess-bishop',
                    ]),
                ],
            ),
            new SectionItem(
                'T2 Advanced Ships',
                [
                    'space-shuttle',
                    'angle-right',
                ],
                [
                    new SectionItem('Frigate', [
                        'chess-pawn',
                        'star',
                    ]),
                    new SectionItem('Destroyer', [
                        'chess-pawn',
                        'star',
                    ]),
                    new SectionItem('Cruiser', [
                        'chess-knight',
                        'star',
                    ]),
                    new SectionItem('Battlecruiser', [
                        'chess-knight',
                        'star',
                    ]),
                    new SectionItem('Battleship', [
                        'chess-bishop',
                        'star',
                    ]),
                ],
            ),
            new SectionItem(
                'T3 Strategic Cruisers',
                [
                    'space-shuttle',
                    'angle-up',
                ],
                [
                    new SectionItem('Amarr', [
                        'church',
                    ]),
                    new SectionItem('Caldari', [
                        'dollar-sign',
                    ]),
                    new SectionItem('Gallente', [
                        'balance-scale',
                    ]),
                    new SectionItem('Minmatar', [
                        'tape',
                    ]),
                ],
            ),
            new SectionItem(
                'Ship Support',
                [
                    'space-shuttle',
                    'plus',
                ],
                [
                    new SectionItem('Fitting', [
                        'tools',
                    ]),
                    new SectionItem('Overheating', [
                        'fire',
                    ]),
                ],
            ),
            new SectionItem(
                'Capital Ships',
                [
                    'space-shuttle',
                    'angle-double-up',
                ],
                [
                    new SectionItem('Dreadnought', [
                        'chess-rook',
                    ]),
                    new SectionItem('Carrier', [
                        'chess-queen',
                    ]),
                    new SectionItem('Titan', [
                        'chess-king',
                    ]),
                ],
            ),
            new SectionItem(
                'Capital Support',
                [
                    'angle-double-up',
                    'plus',
                ],
                [
                    new SectionItem('Piloting', [
                        'location-arrow',
                    ]),
                    new SectionItem('Jump', [
                        'route',
                    ]),
                    new SectionItem('Carrier', [
                        'fighter-jet',
                    ]),
                ],
            ),
        ]);
    }
}
