<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

class WeaponSection extends Section
{
    public function __construct()
    {
        parent::__construct([
            new SectionItem(
                'Gunnery',
                ['compress-arrows-alt'],
                [
                    new SectionItem('Small', ['angle-down']),
                    new SectionItem('Medium', ['angle-right']),
                    new SectionItem('Large', ['angle-up']),
                    new SectionItem('Capital', ['angle-double-up']),
                    new SectionItem('Vorton', ['bolt']),
                    new SectionItem('Support', ['compress-arrows-alt', 'plus']),
                ],
            ),
            new SectionItem(
                'Missiles',
                ['bomb'],
                [
                    new SectionItem('Small', ['angle-down']),
                    new SectionItem('Medium', ['angle-right']),
                    new SectionItem('Large', ['angle-up']),
                    new SectionItem('Capital', ['angle-double-up']),
                    new SectionItem('Support', ['bomb', 'plus']),
                ],
            ),
            new SectionItem(
                'Drones',
                ['robot'],
                [
                    new SectionItem('Operation', ['gamepad']),
                    new SectionItem('Specialization', ['search-plus']),
                    new SectionItem('Support', ['robot', 'plus']),
                ],
            ),
        ]);
    }
}
