<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

class TankSection extends Section
{
    public function __construct()
    {
        parent::__construct([
            new SectionItem(
                'Tank',
                ['globe'],
                [
                    new SectionItem('Armor', ['hard-hat']),
                    new SectionItem('Armor Comp.', ['hard-hat', 'plus']),
                    new SectionItem('Shields', ['shield-alt']),
                    new SectionItem('Shield Comp.', ['shield-alt', 'plus']),
                ],
            ),
        ]);
    }
}
