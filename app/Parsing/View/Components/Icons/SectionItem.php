<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

use Spatie\LaravelData\Data;

class SectionItem extends Data
{
    /**
     * @param  non-empty-string  $name
     * @param  non-empty-array<string>  $icons
     * @param  SectionItem[]  $subsections
     */
    public function __construct(
        public readonly string $name,
        public readonly array $icons,
        public readonly ?array $subsections = null,
    ) {}
}
