<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Icons;

class MiscSection extends Section
{
    public function __construct()
    {
        parent::__construct([
            new SectionItem(
                'Misc',
                ['star'],
                [
                    new SectionItem('Scanning', ['crosshairs']),
                    new SectionItem('Clones & Drugs', ['syringe']),
                ],
            )],
        );
    }
}
