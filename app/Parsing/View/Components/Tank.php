<?php

declare(strict_types=1);

namespace App\Parsing\View\Components;

use App\Parsing\Skills\Skill;
use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Tank extends Component
{
    /**
     * @var Collection<int, Skill>
     */
    protected Collection $armor;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $armor_compensation;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $shields;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $shields_compensation;

    public function parse(): Collection
    {
        $this->parseArmor();
        $this->parseShields();

        return collect([
            'Armor' => $this->armor,
            'Armor Comp.' => $this->armor_compensation,
            'Shields' => $this->shields,
            'Shield Comp.' => $this->shields_compensation,
        ]);
    }

    private function parseArmor(): void
    {
        if ($this->allSkillGroups->has('Armor')) {
            $armorSkills = $this->allSkillGroups->get('Armor');
            $this->armor = $armorSkills
                ->skills
                ->filter(fn (Skill $skill): bool => in_array(
                    $skill->skillName,
                    [
                        'Mechanics',
                        'Hull Upgrades',
                        'Repair Systems',
                        'Armor Layering',
                    ],
                    true,
                ))
                ->sortBy('skillName');
            $this->armor_compensation = $armorSkills
                ->skills
                ->filter($this->compensationFilter())
                ->sortBy('skillName');
        }
    }

    private function parseShields(): void
    {
        if ($this->allSkillGroups->has('Shields')) {
            $shieldSkills = $this->allSkillGroups->get('Shields');
            $this->shields = $shieldSkills
                ->skills
                ->filter(fn (Skill $skill): bool => in_array(
                    $skill->skillName,
                    [
                        'Shield Compensation',
                        'Shield Operation',
                        'Shield Management',
                        'Shield Upgrades',
                    ],
                    true,
                ))
                ->sortBy('skillName');
            $this->shields_compensation = $shieldSkills
                ->skills
                ->filter($this->compensationFilter())
                ->sortBy('skillName');
        }
    }

    /**
     * @return Closure(Skill): bool
     */
    private function compensationFilter(): callable
    {
        return static fn (Skill $skill): bool => Str::contains($skill->skillName, [
            'EM',
            'Thermal',
            'Kinetic',
            'Explosive',
        ]) && Str::endsWith($skill->skillName, 'Compensation');
    }
}
