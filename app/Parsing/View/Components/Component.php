<?php

declare(strict_types=1);

namespace App\Parsing\View\Components;

use App\Parsing\Skills\Skill;
use App\Parsing\Skills\SkillGroup;
use Illuminate\Support\Collection;

abstract class Component
{
    /**
     * @var string[]
     */
    protected array $empireFactions = [
        'Amarr',
        'Caldari',
        'Gallente',
        'Minmatar',
    ];

    /**
     * @var string[]
     */
    protected array $extraFactions = [
        'Precursor',
        'EDENCOM',
    ];

    /**
     * Populated in __construct.
     *
     * @var string[]
     */
    protected array $allFactions = [];

    /**
     * @var string[]
     */
    protected array $subcapTypes = [
        'Frigate',
        'Destroyer',
        'Cruiser',
        'Battlecruiser',
        'Battleship',
        'T3 Cruiser',
    ];

    /**
     * @var string[]
     */
    protected array $capitalTypes = [
        'Dreadnought',
        'Carrier',
        'Titan',
        'Support',
    ];

    /**
     * @param  Collection<string, SkillGroup>  $allSkillGroups
     */
    public function __construct(
        protected readonly Collection $allSkillGroups,
        protected readonly ?SkillGroup $groupSkills = null,
    ) {
        $this->allFactions = array_merge($this->empireFactions, $this->extraFactions);
    }

    /**
     * Where child components should do any prep work for their skills.
     *
     * @return Collection<string, Collection<int, Skill>>
     */
    abstract public function parse(): Collection;

    /**
     * @param  list<non-falsy-string>  $skills
     * @return Collection<int, Skill>
     */
    protected function getFilteredSortedSkills(
        array $skills
    ): Collection {
        return $this->groupSkills
            ->skills
            ->filter(static fn (Skill $skill): bool => in_array(
                $skill->skillName,
                $skills,
                true,
            ))
            ->sortBy(static fn (Skill $skill): int|false => array_search(
                $skill->skillName,
                $skills,
                true,
            ));
    }
}
