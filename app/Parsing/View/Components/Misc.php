<?php

declare(strict_types=1);

namespace App\Parsing\View\Components;

use Illuminate\Support\Collection;

class Misc extends Component
{
    public function parse(): Collection
    {
        return collect([
            'Scanning' => $this->allSkillGroups->get('Scanning')->skills->sortKeys(),
            'Clones & Drugs' => $this->allSkillGroups->get('Neural Enhancement')
                ->skills->sortKeys(),
        ]);
    }
}
