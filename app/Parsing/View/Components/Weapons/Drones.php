<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Weapons;

use App\Parsing\Skills\Skill;
use App\Parsing\View\Components\Component;
use Illuminate\Support\Collection;

class Drones extends Component
{
    public function parse(): Collection
    {
        return collect([
            'Operation' => $this->parseOperation(),
            'Support' => $this->parseSupport(),
            'Specialization' => $this->parseSpecialization(),
        ]);
    }

    /**
     * @return Collection<int, Skill>
     */
    private function parseOperation(): Collection
    {
        $operationSkills = collect([
            'Light',
            'Medium',
            'Heavy',
            'Mining',
            'Ice Harvesting',
        ])
            ->map(fn (string $type): string => "$type Drone Operation")
            ->push('Sentry Drone Interfacing');

        return $this->getFilteredSortedSkills($operationSkills->toArray());
    }

    /**
     * @return Collection<int, Skill>
     */
    private function parseSpecialization(): Collection
    {
        $specializationSkills = collect($this->empireFactions)
            ->push('Mining', 'Ice Harvesting')
            ->map(fn (string $type): string => "$type Drone Specialization");

        return $this->getFilteredSortedSkills($specializationSkills->toArray());
    }

    /**
     * @return Collection<int, Skill>
     */
    private function parseSupport(): Collection
    {
        $supportSkills = collect([
            'Avionics',
            'Durability',
            'Interfacing',
            'Navigation',
            'Sharpshooting',
        ])
            ->map(fn (string $skill): string => "Drone $skill")
            ->prepend('Drones')
            ->push('Advanced Drone Avionics');

        return $this->getFilteredSortedSkills($supportSkills->toArray());
    }
}
