<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Weapons;

use App\Parsing\Skills\Skill;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Gunnery extends Weapon
{
    /**
     * @const array<string, array<string>>
     */
    private const array OPERATIONS = [
        'Energy' => [
            'Pulse Laser',
            'Beam Laser',
        ],
        'Hybrid' => [
            'Blaster',
            'Railgun',
        ],
        'Projectile' => [
            'Autocannon',
            'Artillery',
        ],
    ];

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $vorton;

    public function parse(): Collection
    {
        $this->parseOperation();
        $this->parseVortonOperation();
        $this->parseSupport();

        return collect([
            'Small' => $this->small,
            'Medium' => $this->medium,
            'Large' => $this->large,
            'Capital' => $this->capital,
            'Vorton' => $this->vorton,
            'Support' => $this->support,
        ])->recursive();
    }

    public function parseOperation(): void
    {
        collect(['Small', 'Medium', 'Large', 'Capital'])
            ->each(function (string $size): void {
                $skills = collect(self::OPERATIONS)
                    ->recursive()
                    ->map(fn (Collection $specializations, string $type): array => [
                        sprintf('%s %s Turret', $size, $type),
                        $specializations->map(
                            fn ($specialization): string => sprintf(
                                '%s %s Specialization',
                                $size,
                                $specialization,
                            )
                        ),
                    ])
                    ->flatten()
                    ->push([
                        sprintf('%s Precursor Weapon', $size),
                        sprintf('%s Disintegrator Specialization', $size),
                    ]);

                $prop = Str::lower($size);
                $this->$prop = $this->getFilteredSortedSkills($skills->toArray());
            });
    }

    public function parseSupport(): void
    {
        $this->support = $this->getFilteredSortedSkills([
            'Controlled Bursts',
            'Gunnery',
            'Motion Prediction',
            'Rapid Firing',
            'Sharpshooter',
            'Surgical Strike',
            'Trajectory Analysis',
        ]);
    }

    private function parseVortonOperation(): void
    {
        $vorton = collect([
            'Small',
            'Medium',
            'Large',
        ])
            ->crossJoin(['Projector', 'Specialization'])
            /** @phpstan-ignore-next-line */
            ->mapSpread(fn (string $size, string $skill): string => $size && $skill
                ? "$size Vorton $skill"
                : ''
            )
            ->push(
                'Vorton Arc Extension',
                'Vorton Arc Guidance',
                'Vorton Power Amplification',
                'Vorton Projector Operation',
            );

        $this->vorton = $this->getFilteredSortedSkills($vorton->toArray());
    }
}
