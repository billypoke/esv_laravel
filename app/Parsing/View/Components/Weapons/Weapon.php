<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Weapons;

use App\Parsing\Skills\Skill;
use App\Parsing\View\Components\Component;
use Illuminate\Support\Collection;

abstract class Weapon extends Component
{
    /**
     * @var Collection<int, Skill>
     */
    protected Collection $medium;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $small;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $capital;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $large;

    /**
     * @var Collection<int, Skill>
     */
    protected Collection $support;

    /**
     * Parse and assign the operation skills to the class vars.
     */
    abstract public function parseOperation(): void;

    /**
     * Parse and assign the operation skills to the class vars.
     */
    abstract public function parseSupport(): void;
}
