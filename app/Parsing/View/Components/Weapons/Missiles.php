<?php

declare(strict_types=1);

namespace App\Parsing\View\Components\Weapons;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Missiles extends Weapon
{
    /**
     * @const array<string, array<string>>
     */
    private const array GROUPS = [
        'small' => [
            'Rocket',
            'Light Missile',
        ],
        'medium' => [
            'Heavy Assault Missile',
            'Heavy Missile',
        ],
        'large' => [
            'Torpedoe', // Funny spelling for pluralizer
            'Torpedo',
            'Cruise Missile',
        ],
        'capital' => [
            'XL Torpedoe', // Funny spelling for pluralizer
            'XL Torpedo',
            'XL Cruise Missile',
        ],
    ];

    public function parse(): Collection
    {
        $this->parseOperation();
        $this->parseSupport();

        return collect([
            'Small' => $this->small,
            'Medium' => $this->medium,
            'Large' => $this->large,
            'Capital' => $this->capital,
            'Support' => $this->support,
        ])->recursive();
    }

    public function parseOperation(): void
    {
        foreach (self::GROUPS as $size => $skills) {
            $this->$size = $this->getFilteredSortedSkills(
                collect($skills)
                    ->map($this->getSkillAndSpec())
                    ->flatten()
                    ->toArray()
            );
        }
    }

    public function parseSupport(): void
    {
        $this->support = $this->getFilteredSortedSkills([
            'Guided Missile Precision',
            'Missile Bombardment',
            'Missile Launcher Operation',
            'Missile Projection',
            'Rapid Launch',
            'Target Navigation Prediction',
            'Warhead Upgrades',
        ]);
    }

    /**
     * @return Closure(string): array<string>
     */
    private function getSkillAndSpec(): callable
    {
        return static fn (string $type): array => [
            Str::plural($type),
            "$type Specialization",
        ];
    }
}
