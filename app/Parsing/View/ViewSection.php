<?php

declare(strict_types=1);

namespace App\Parsing\View;

use App\Parsing\Skills\Skill;
use App\Parsing\View\Components\Icons\Section;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class ViewSection extends Data
{
    /**
     * @param  Collection<string, Collection<int, Skill>>  $skills
     */
    public function __construct(
        public readonly string $name,
        public readonly Collection $skills,
        public readonly Section $section,
    ) {}
}
