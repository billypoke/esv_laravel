<?php

declare(strict_types=1);

namespace App\Parsing\View;

use App\Parsing\Skills\SkillGroup;
use App\Parsing\View\Components\Icons\MiscSection;
use App\Parsing\View\Components\Icons\ShipSection;
use App\Parsing\View\Components\Icons\TankSection;
use App\Parsing\View\Components\Icons\WeaponSection;
use App\Parsing\View\Components\Misc;
use App\Parsing\View\Components\Ships;
use App\Parsing\View\Components\Tank;
use App\Parsing\View\Components\Weapons\Drones;
use App\Parsing\View\Components\Weapons\Gunnery;
use App\Parsing\View\Components\Weapons\Missiles;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class ViewSections extends Data
{
    /**
     * @var Collection<int, ViewSection|GroupedViewSection>
     */
    public readonly Collection $sections;

    /**
     * @param  Collection<string, SkillGroup>  $skills
     */
    public function __construct(
        Collection $skills,
    ) {
        $this->sections = collect([
            new ViewSection(
                'ships',
                (new Ships(
                    $skills,
                    $skills->get('Spaceship Command'))
                )->parse(),
                new ShipSection
            ),
            new GroupedViewSection(
                'weapons',
                collect([
                    'Gunnery' => (new Gunnery(
                        $skills,
                        $skills->get('Gunnery'))
                    )->parse(),
                    'Missiles' => (new Missiles(
                        $skills,
                        $skills->get('Missiles'))
                    )->parse(),
                    'Drones' => (new Drones(
                        $skills,
                        $skills->get('Drones'))
                    )->parse(),
                ]),
                new WeaponSection,
            ),
            new ViewSection(
                'tank',
                (new Tank($skills))->parse(),
                new TankSection,
            ),
            new ViewSection(
                'misc',
                (new Misc($skills))->parse(),
                new MiscSection,
            ),
        ]);
    }
}
