<?php

declare(strict_types=1);

namespace App\Parsing\SkillQueue;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class SkillQueue extends Data
{
    /**
     * @param  Collection<int, non-falsy-string>  $queuedSkillNames
     */
    public function __construct(
        public readonly ?CurrentSkill $currentSkill,
        public readonly int $skillsInQueueCount,
        public readonly ?Carbon $totalTime,
        public readonly Collection $queuedSkillNames,
    ) {}
}
