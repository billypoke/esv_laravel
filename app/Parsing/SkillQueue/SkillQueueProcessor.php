<?php

declare(strict_types=1);

namespace App\Parsing\SkillQueue;

use App\Esi\Entities\QueuedSkill;
use App\Esi\EsiResponse;
use App\Esi\SkillTypeInformation;
use App\Parsing\Contracts\DataProcessor;
use Illuminate\Support\Carbon;

class SkillQueueProcessor implements DataProcessor
{
    public function parse(SkillTypeInformation $typeInformation, EsiResponse $skillsAndQueue): SkillQueue
    {
        $activeSkill = $skillsAndQueue->skillQueue->first();
        $skillsInQueueCount = $skillsAndQueue->skillQueue->count();

        $now = now('UTC');

        // Deal with any skills that have finished training but are still in the queue
        $currentSkill = null;
        $totalTime = null;

        [$activeSkill, $skillsInQueueCount] = $this->determineActiveSkillAndCount(
            $activeSkill,
            $now,
            $skillsAndQueue,
            $skillsInQueueCount
        );

        if ($this->skillCanBeParsed($activeSkill)) {
            $startTimestamp = Carbon::parse($activeSkill['start_date']);
            $finishTimestamp = Carbon::parse($activeSkill['finish_date']);
            $totalSeconds = $skillsAndQueue->skillQueue->sum(
                fn (QueuedSkill $queuedSkill): float => Carbon::parse($queuedSkill->finishDate)
                    ->diffInSeconds($now->max($queuedSkill->startDate))
            );
            $totalTime = $now->clone()->addSeconds($totalSeconds);

            $currentSkill = new CurrentSkill(
                startDatetime: $startTimestamp->toDateTimeString(),
                finishDatetime: $finishTimestamp->toDateTimeString(),
                completedPct: $now->diffInSeconds($startTimestamp) /
                $finishTimestamp->diffinSeconds($startTimestamp) * 100,
                skillName: $typeInformation->skillNames->get($activeSkill->skillId),
                skillLevel: $activeSkill['finished_level'],
            );
        }

        return new SkillQueue(
            currentSkill: $currentSkill,
            skillsInQueueCount: $skillsInQueueCount,
            totalTime: $totalTime,
            queuedSkillNames: $skillsAndQueue->skillQueue->map(fn (QueuedSkill $skill): string => sprintf(
                '%s (Level %s)',
                $typeInformation->skillNames->get($skill->skillId),
                $skill->finishedLevel,
            )),
        );
    }

    /**
     * @return array{0: ?QueuedSkill, 1: int}
     */
    protected function determineActiveSkillAndCount(
        ?QueuedSkill $activeSkill,
        Carbon $now,
        EsiResponse $skillsAndQueue,
        int $skillsInQueueCount
    ): array {
        while (
            $activeSkill &&
            $activeSkill->finishDate &&
            $now->isAfter(Carbon::parse($activeSkill->finishDate, 'UTC'))
        ) {
            $activeSkill = $skillsAndQueue->skillQueue->get(1);
            $skillsInQueueCount--;
            $skillsAndQueue->skillQueue->shift();
        }

        return [$activeSkill, $skillsInQueueCount];
    }

    private function skillCanBeParsed(?QueuedSkill $activeSkill): bool
    {
        if (!$activeSkill instanceof QueuedSkill) {
            return false;
        }

        return array_all(
            [
                'start_date',
                'finish_date',
                'skill_id',
                'finished_level',
            ],
            static fn ($item): bool => array_key_exists($item, $activeSkill->toArray())
        );
    }
}
