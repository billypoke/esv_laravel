<?php

declare(strict_types=1);

namespace App\Parsing\SkillQueue;

use Spatie\LaravelData\Data;

class CurrentSkill extends Data
{
    public function __construct(
        public string $startDatetime,
        public string $finishDatetime,
        public float $completedPct,
        public string $skillName,
        public int $skillLevel,
    ) {}
}
