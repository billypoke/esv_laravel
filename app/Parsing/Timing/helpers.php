<?php

declare(strict_types=1);

namespace App\Parsing\Timing;

function toMillis(float $seconds, bool $formatted = true): float|string
{
    $millis = $seconds * 1000;

    return $formatted ? number_format($millis, 2) : $millis;
}
