<?php

declare(strict_types=1);

namespace App\Esi;

use App\Esi\Concerns\BuildsEsiUris;
use Illuminate\Http\Client\Pool;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

readonly class InvRetriever
{
    use BuildsEsiUris;

    public function __construct(
        private EsiClient $esi
    ) {}

    public function skillTypeInformation(): SkillTypeInformation
    {
        $start = microtime(true);

        $skillCategoryId = config('services.eveonline.category_skills');

        $categoriesResponse = $this->esi
            ->pendingRequestWithMiddleware()
            ->get($this->universe('categories', $skillCategoryId));

        $groupResponses = Http::pool(
            fn (Pool $pool) => $categoriesResponse
                ->collect('groups')
                ->map(
                    fn (int $groupId) => $pool
                        ->withMiddleware($this->esi->cacheMiddleware())
                        ->get($this->universe(
                            'groups',
                            $groupId,
                        )),
                )
                ->toArray(),
        );

        return new SkillTypeInformation(
            skillGroups: collect($groupResponses)->mapWithKeys(
                fn (Response $response) => [
                    (string) $response->json('name') => $response
                        ->collect('types')
                        ->mapWithKeys(
                            fn (int $typeId, int $index) => [$index => $typeId],
                        ),
                ],
            ),
            skillNames: $this->resolveSkillNames(
                $this->resolveSkillIds($groupResponses),
                $categoriesResponse,
            ),
            typeTime: microtime(true) - $start,
        );
    }

    /**
     * @param  int[]  $ids
     * @return Collection<int, non-falsy-string>
     */
    protected function resolveSkillNames(
        array $ids,
        Response $skillCategory,
    ): Collection {
        // Since we're fetching all skill groups, we can cache all the names until the category expires
        return Cache::remember(
            hash('sha256', serialize($ids)),
            Carbon::parse($skillCategory->header('Expires'), 'UTC'),
            fn () => $this->esi
                ->pendingRequestWithMiddleware()
                ->post(
                    $this->universe('names'),
                    $ids,
                )
                ->throw()
                ->collect()
                ->pluck('name', 'id'),
        );
    }

    /**
     * @param  Response[]  $groupResponses
     * @return int[]
     */
    protected function resolveSkillIds(array $groupResponses): array
    {
        return collect($groupResponses)
            ->mapWithKeys(
                fn (Response $response) => [
                    $response->json('name') => $response->collect('types'),
                ],
            )
            ->flatten()
            ->values()
            ->toArray();
    }
}
