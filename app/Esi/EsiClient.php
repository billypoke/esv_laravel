<?php

declare(strict_types=1);

namespace App\Esi;

use App\Esi\Entities\QueuedSkill;
use App\Esi\Entities\Skill;
use App\Traits\RetrievesCharacters;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Collection;

class EsiClient
{
    use RetrievesCharacters;

    /**
     * @return Collection<int|string, mixed>
     *
     * @throws RequestException
     * @throws ConnectionException
     */
    public function query(string $endpoint): Collection
    {
        return $this->pendingRequestWithMiddleware()
            ->get(sprintf(
                '%s/%s',
                config('services.eveonline.base_url'),
                $endpoint,
            ))
            ->throw()
            ->collect();
    }

    public function skillsAndQueue(string $token, int $characterId): EsiResponse
    {
        $start = microtime(true);

        /** @var array{
         *     'skills': array{
         *          'active_skill_level': int,
         *          'skill_id': int,
         *          'skillpoints_in_skill': int,
         *          'trained_skill_level': int,
         *     },
         *     'total_sp': int,
         *     'unallocated_sp': int,
         * } $skillsObj
         */
        $skillsObj = $this->characterData(
            $characterId,
            'skills',
            $token,
        );

        return new EsiResponse(
            skillsById: Skill::collect(
                $skillsObj['skills'],
                Collection::class
            )->keyBy('skillId'),
            skillQueue: QueuedSkill::collect(
                $this->characterData(
                    $characterId,
                    'skillqueue',
                    $token,
                ),
                Collection::class
            ),
            totalSp: $skillsObj['total_sp'],
            elapsed: microtime(true) - $start,
        );
    }

    /**
     * @return array<int|string, mixed>
     *
     * @throws RequestException
     * @throws ConnectionException
     */
    public function characterData(
        int $characterId,
        string $endpoint,
        string $token
    ): array {
        return $this->authenticatedQuery($token, sprintf(
            'characters/%s/%s',
            $characterId,
            $endpoint
        ));
    }

    /**
     * @return array<int|string, mixed>
     *
     * @throws ConnectionException
     * @throws RequestException
     */
    public function authenticatedQuery(
        string $token,
        string $endpoint,
    ): array {
        return tap(
            $this->pendingRequestWithMiddleware(),
            static fn (PendingRequest $request) => $request->withToken($token)
        )
            ->get(sprintf(
                '%s/%s',
                config('services.eveonline.base_url'),
                $endpoint,
            ))
            ->throw()
            ->json();
    }
}
