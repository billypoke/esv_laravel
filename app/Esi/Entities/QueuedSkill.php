<?php

declare(strict_types=1);

namespace App\Esi\Entities;

use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Data;

/**
 * https://esi.evetech.net/ui/#/Skills/get_characters_character_id_skillqueue
 */
class QueuedSkill extends Data
{
    public function __construct(
        #[MapInputName('finish_date')]
        public ?string $finishDate,
        #[MapInputName('finished_level')]
        public int $finishedLevel,
        #[MapInputName('level_end_sp')]
        public ?int $levelEndSp,
        #[MapInputName('level_start_sp')]
        public ?int $levelStartSp,
        #[MapInputName('queue_position')]
        public int $queuePosition,
        #[MapInputName('skill_id')]
        public int $skillId,
        #[MapInputName('start_date')]
        public ?string $startDate,
        #[MapInputName('training_start_sp')]
        public int $trainingStartSp,
    ) {}
}
