<?php

declare(strict_types=1);

namespace App\Esi\Entities;

use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\Data;

/**
 * https://esi.evetech.net/ui/#/Skills/get_characters_character_id_skills
 */
class Skill extends Data
{
    public function __construct(
        #[MapInputName('active_skill_level')]
        public readonly int $activeSkillLevel,
        #[MapInputName('skill_id')]
        public readonly int $skillId,
        #[MapInputName('skillpoints_in_skill')]
        public readonly int $skillpointsInSkill,
        #[MapInputName('trained_skill_level')]
        public readonly int $trainedSkillLevel,
    ) {}
}
