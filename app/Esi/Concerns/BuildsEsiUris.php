<?php

declare(strict_types=1);

namespace App\Esi\Concerns;

trait BuildsEsiUris
{
    protected function universe(string $endpoint, ?int $id = null): string
    {
        $uri = sprintf(
            '%s/universe/%s',
            config('services.eveonline.base_url'),
            $endpoint,
        );

        if ($id) {
            $uri .= '/'.$id;
        }

        return $uri;
    }
}
