<?php

declare(strict_types=1);

namespace App\Esi;

use Laravel\Socialite\Two\User as OAuthTwoUser;

/**
 * @property-read string $character_owner_hash
 * @property-read string $character_name
 * @property-read string $character_id
 */
class User extends OAuthTwoUser {}
