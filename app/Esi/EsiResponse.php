<?php

declare(strict_types=1);

namespace App\Esi;

use App\Esi\Entities\QueuedSkill;
use App\Esi\Entities\Skill;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class EsiResponse extends Data
{
    /**
     * @param  Collection<int, Skill>  $skillsById
     * @param  Collection<int, QueuedSkill>  $skillQueue
     */
    public function __construct(
        public Collection $skillsById,
        public Collection $skillQueue,
        public int $totalSp,
        public float $elapsed,
    ) {}
}
