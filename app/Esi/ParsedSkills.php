<?php

declare(strict_types=1);

namespace App\Esi;

use App\Parsing\Skills\SkillGroup;
use App\Parsing\Stats\StatsGroup;
use App\Parsing\View\GroupedViewSection;
use App\Parsing\View\ViewSection;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class ParsedSkills extends Data
{
    /**
     * @param  Collection<int, StatsGroup>  $stats
     * @param  Collection<string, SkillGroup>  $skills
     * @param  Collection<int, ViewSection|GroupedViewSection>  $sections
     */
    public function __construct(
        public Collection $stats,
        public Collection $skills,
        public Collection $sections,
        public Summary $summary,
    ) {}
}
