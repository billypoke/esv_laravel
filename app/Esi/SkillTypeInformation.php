<?php

declare(strict_types=1);

namespace App\Esi;

use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class SkillTypeInformation extends Data
{
    /**
     * @param  Collection<string, Collection<int, int>>  $skillGroups
     * @param  Collection<int, non-falsy-string>  $skillNames
     */
    public function __construct(
        public Collection $skillGroups,
        public Collection $skillNames,
        public float $typeTime,
    ) {}
}
