<?php

declare(strict_types=1);

namespace App\Esi;

use App\Parsing\SkillQueue\SkillQueueProcessor;
use App\Parsing\Skills\SkillsProcessor;
use App\Parsing\Stats\SkillStatsProcessor;
use App\Parsing\View\ViewSections;
use Illuminate\Http\Client\RequestException;

use function App\Parsing\Timing\toMillis;

readonly class SkillParser
{
    public function __construct(
        private EsiClient $esi,
        private InvRetriever $retriever,
        private SkillQueueProcessor $queue,
        private SkillStatsProcessor $stats,
        private SkillsProcessor $skills,
    ) {
    }

    /**
     * @throws RequestException
     */
    public function parse(
        string $token,
        int $characterId,
        float $userTime
    ): ParsedSkills {
        $skillsAndQueue = $this->esi->skillsAndQueue($token, $characterId);

        $typeInformation = $this->retriever->skillTypeInformation();

        $parseStart = microtime(true);

        $stats = $this->stats->parse($typeInformation, $skillsAndQueue)->statsGroups;

        $skills = $this->skills->parse($typeInformation, $skillsAndQueue)->groupedSkills;

        $viewData = new ViewSections($skills);

        $skillQueueData = $this->queue->parse($typeInformation, $skillsAndQueue);

        return new ParsedSkills(
            stats: $stats,
            skills: $skills,
            sections: $viewData->sections,
            summary: new Summary(
                totalSp: number_format($skillsAndQueue->totalSp),
                numSkills: $skillsAndQueue->skillsById->count(),
                userTime: toMillis($userTime),
                esiTime: toMillis($skillsAndQueue->elapsed),
                parseTime: toMillis(microtime(true) - $parseStart),
                typeTime: toMillis($typeInformation->typeTime),
                currentSkill: $skillQueueData->currentSkill,
                skillsInQueueCount: $skillQueueData->skillsInQueueCount,
                skillQueue: $skillQueueData->queuedSkillNames,
                totalTime: $skillQueueData->totalTime?->toDateTimeString(),
                timeRemaining: $skillQueueData->totalTime?->diffForHumans(),
            )
        );
    }
}
