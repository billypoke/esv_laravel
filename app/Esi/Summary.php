<?php

declare(strict_types=1);

namespace App\Esi;

use App\Parsing\SkillQueue\CurrentSkill;
use Illuminate\Support\Collection;
use Spatie\LaravelData\Data;

class Summary extends Data
{
    /**
     * @param  Collection<int, non-falsy-string>  $skillQueue
     */
    public function __construct(
        public readonly string $totalSp,
        public readonly int $numSkills,
        public readonly string $userTime,
        public readonly string $esiTime,
        public readonly string $parseTime,
        public readonly string $typeTime,
        public readonly ?CurrentSkill $currentSkill,
        public readonly int $skillsInQueueCount,
        public readonly Collection $skillQueue,
        public readonly ?string $totalTime = 'Queue Paused',
        public readonly ?string $timeRemaining = 'Queue Paused',
    ) {}
}
