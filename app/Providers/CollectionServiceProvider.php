<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class CollectionServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Collection::macro(
            'recursive',
            fn () => $this->map(
                fn ($value) => $value instanceof Arrayable || is_iterable($value)
                    ? collect($value)->recursive()
                    : $value
            )
        );
    }
}
