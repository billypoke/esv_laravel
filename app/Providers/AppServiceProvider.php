<?php

declare(strict_types=1);

namespace App\Providers;

use App\Models\User;
use Barryvdh\Debugbar\ServiceProvider as DebugBarServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Vite;
use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\Facades\Socialite;
use Override;
use SocialiteProviders\Eveonline\Provider;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\SocialiteWasCalled;

class AppServiceProvider extends ServiceProvider
{
    #[Override]
    public function register(): void
    {
        if (config('app.debug', false)) {
            $this->app->register(DebugBarServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if (config('app.debug', false)) {
            DB::enableQueryLog();
        }

        $this->app->singleton(
            AbstractProvider::class,
            fn () => Socialite::driver('eveonline'),
        );

        Vite::prefetch(concurrency: 3);

        Gate::define(
            'viewPulse',
            static fn (User $user): bool => $user->isAdmin(),
        );

        Event::listen(static function (SocialiteWasCalled $event): void {
            $event->extendSocialite(
                'eveonline',
                Provider::class,
            );
        });
    }
}
