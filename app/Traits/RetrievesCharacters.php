<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Token;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\LaravelCacheStorage;
use Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy;
use Laravel\Socialite\Two\User;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;

trait RetrievesCharacters
{
    public static function bootRetrievesCharacters(): void
    {
        // Increase leeway for expired tokens
        JWT::$leeway = 10;
    }

    /**
     * @throws RequestException
     */
    protected function retrieveStoredCharacter(Token $token): User
    {
        return Cache::remember(
            hash('sha256', $token->access_token),
            20 * 60,
            function () use ($token) {
                try {
                    return app(AbstractProvider::class)->userFromToken(
                        $token->access_token,
                    );
                } catch (ExpiredException) {
                    return $this->handleTokenExpired($token);
                }
            });
    }

    /**
     * @throws RequestException
     */
    protected function handleTokenExpired(Token $token): User
    {
        $attrs = $this->getUpdatedToken($token);
        $token->update($attrs);

        return app(AbstractProvider::class)->userFromToken(
            $attrs['access_token'],
        );
    }

    /**
     * @return array<string>
     *
     * @throws RequestException
     */
    protected function getUpdatedToken(Token $token): array
    {
        return $this->pendingRequestWithMiddleware()
            ->withBasicAuth(
                config('services.eveonline.client_id'),
                config('services.eveonline.client_secret'),
            )
            ->withHeaders(['Host' => 'login.eveonline.com'])
            ->asForm()
            ->post(config('services.eveonline.token_url'), [
                'grant_type' => 'refresh_token',
                'refresh_token' => $token->refresh_token,
            ])
            ->throw()
            ->collect()
            ->only(['access_token', 'refresh_token', 'expires_in'])
            ->toArray();
    }

    public function pendingRequestWithMiddleware(): PendingRequest
    {
        return Http::withMiddleware($this->cacheMiddleware());
    }

    public function cacheMiddleware(): CacheMiddleware
    {
        return new CacheMiddleware(
            new PrivateCacheStrategy(
                new LaravelCacheStorage(
                    Cache::store(config('cache.default'))
                )
            )
        );
    }
}
