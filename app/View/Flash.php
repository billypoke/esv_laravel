<?php

declare(strict_types=1);

namespace App\View;

enum Flash: string
{
    case Danger = 'danger';
    case Warning = 'warning';
    case Success = 'success';
    case Info = 'info';
}
