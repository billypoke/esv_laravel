<?php

declare(strict_types=1);

namespace App\Listeners;

use Illuminate\Http\Client\Events\RequestSending;
use Illuminate\Http\Client\Events\ResponseReceived;

class HttpClientListener
{
    public function handle(RequestSending|ResponseReceived $event): void
    {
        if (config('app.debug', false)) {
            $url = $event->request->url();

            match($event::class) {
                RequestSending::class => debugbar()->startMeasure($url),
                ResponseReceived::class => debugbar()->stopMeasure($event->request->url()),
            };
        }
    }
}
