<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Esi\EsiClient;
use App\Models\Token;
use App\Traits\RetrievesCharacters;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Http\Client\RequestException;

class CacheSkillQueue implements ShouldQueue
{
    use Queueable;
    use RetrievesCharacters;

    public function __construct(
        private readonly Token $character,
    ) {}

    public function handle(EsiClient $esi): void
    {
        try {
            $storedCharacter = $this->retrieveStoredCharacter($this->character);
        } catch (RequestException $e) {
            report($e);

            return;
        }

        $esi->characterData(
            (int) $storedCharacter->character_id,
            'skillqueue',
            $storedCharacter->token,
        );
    }
}
