<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\Token;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;

class CacheSavedCharactersSkills implements ShouldQueue
{
    use Queueable;

    public function handle(): void
    {
        Token::query()->get()->each(
            fn (Token $token) => CacheSkills::dispatch($token)
        );
    }
}
