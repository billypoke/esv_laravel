<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Esi\Concerns\BuildsEsiUris;
use App\Esi\EsiClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;
use Illuminate\Http\Client\Pool;
use Illuminate\Support\Facades\Http;

/**
 * Queries the ESI API for the current list of group ids in the skill category.
 * The response is cached by the client's middleware stack.
 */
class CacheEsiGroups implements ShouldQueue
{
    use BuildsEsiUris;
    use Queueable;

    public function handle(EsiClient $esi): void
    {
        $skillCategoryId = config('services.eveonline.category_skills');

        $categoriesResponse = $esi
            ->pendingRequestWithMiddleware()
            ->get($this->universe('categories', $skillCategoryId));

        Http::pool(
            fn (Pool $pool) => $categoriesResponse
                ->collect('groups')
                ->map(
                    fn (int $groupId) => $pool
                        ->withMiddleware($esi->cacheMiddleware())
                        ->get($this->universe(
                            'groups',
                            $groupId,
                        )),
                )
                ->toArray(),
        );
    }
}
