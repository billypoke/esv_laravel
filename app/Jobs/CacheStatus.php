<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Esi\Concerns\BuildsEsiUris;
use App\Esi\EsiClient;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Queue\Queueable;

/**
 * Queries the ESI API for the current status of the server. The response will
 * be cached by the client's middleware stack.
 */
class CacheStatus implements ShouldQueue
{
    use BuildsEsiUris;
    use Queueable;

    public function handle(EsiClient $client): void
    {
        $client->query('status');
    }
}
