<?php

declare(strict_types=1);

use App\Models\Token;
use App\Models\User;
use App\Traits\RetrievesCharacters;
use function Pest\Laravel\actingAs;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;

uses(RetrievesCharacters::class);

beforeEach(function (): void {
    $this->user = User::factory()
        ->create();

    actingAs($this->user);

    $testCharacter = config('services.eveonline.test_character');

    $Token = Token::factory([
        'user_id' => $this->user->getKey(),
        'character_id' => $testCharacter['character_id'],
        'access_token' => $testCharacter['access_token'],
        'refresh_token' => $testCharacter['refresh_token'],
        'expires_in' => 1200,
    ])->create();
    $this->retrieveStoredCharacter($Token);
});

it('can show a list of characters for a user', function (): void {
    $response = getJson(route('characters.index'));

    $response->assertOk();
});

it('can show a character', function (): void {
    $response = getJson(route('characters.show', [
        'character' => $this->user->characters->first(),
    ]));

    $response->assertOk();
});

it('can remove a stored character', function (): void {
    $response = deleteJson(route('characters.destroy', [
        'character' => $this->user->characters->first(),
    ]));

    $response->assertRedirect(route('characters.index'));
});

it("will not delete a character that doesn't belong to the user", function (): void {
    $character = Token::factory()->create();

    $response = deleteJson(route('characters.destroy', [
        'character' => $character->character_id,
    ]));

    $response->assertForbidden();
});
