<?php

declare(strict_types=1);

use App\Models\Token;
use App\Traits\RetrievesCharacters;
use Illuminate\Testing\Fluent\AssertableJson;
use Inertia\Testing\AssertableInertia;
use function Pest\Laravel\getJson;

uses(RetrievesCharacters::class);

it('can view the landing page', function (): void {
    $response = getJson(route('landing'));

    $response->assertOk();
});

it('can view a character', function (): void {
    $token = $this->getUpdatedToken(new Token([
        'refresh_token' => config('services.eveonline.test_character.refresh_token'),
    ]))['access_token'];
    $response = getJson(route('view', ['token' => $token]));
    $response->assertInertia(
        fn (AssertableInertia $inertia): AssertableJson => $inertia->where(
            'character.attributes.character_id',
            config('services.eveonline.test_character.character_id'),
        ),
    );
});
