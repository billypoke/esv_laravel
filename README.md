# EVE ESI Skill Viewer

[![Laravel Forge Site Deployment Status](https://img.shields.io/endpoint?url=https%3A%2F%2Fforge.laravel.com%2Fsite-badges%2F98c5b870-189a-4000-b8ef-18a1c3ab7b09%3Fdate%3D1%26commit%3D1&style=for-the-badge)](https://forge.laravel.com/servers/872216/sites/2573326)

## What is this?

A web app for the game EVE Online to view your skills, grouped into useful
categories. See where you have good coverage, and where you need to train your
skills.
