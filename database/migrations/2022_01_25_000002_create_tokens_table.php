<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('tokens', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('character_id')
                ->references('character_id')
                ->on('characters')
                ->cascadeOnDelete();
            $table->text('access_token');
            $table->string('refresh_token');
            $table->integer('expires_in');
            $table->timestamps();

            $table->unique(['character_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('tokens');
    }
};
