<?php

declare(strict_types=1);

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('characters', function (Blueprint $table): void {
            $table->string('character_owner_hash')->primary();
            $table->foreignIdFor(User::class)
                ->constrained()
                ->cascadeOnDelete();
            $table->integer('character_id')->unique();
            $table->string('character_name');
            $table->timestamps();

            // Each character can only be owned by one user at a time
            $table->unique(['character_owner_hash', 'character_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('characters');
    }
};
