<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Character;
use App\Models\Token;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Token>
 */
class TokenFactory extends Factory
{
    public function definition(): array
    {
        return [
            'character_id' => Character::factory(),
            'access_token' => Str::random(10),
            'refresh_token' => Str::random(10),
            'expires_in' => 20 * 60,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
