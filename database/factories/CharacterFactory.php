<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Character;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Character>
 */
class CharacterFactory extends Factory
{
    public function definition(): array
    {
        return [
            'character_owner_hash' => Str::random(10),
            'user_id' => User::factory(),
            'character_id' => fake()->randomNumber(9),
            'character_name' => Str::random(10),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
