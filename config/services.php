<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'eveonline' => [
        'token_url' => 'https://login.eveonline.com/v2/oauth/token',
        'client_id' => env('ESI_CLIENT_ID'),
        'client_secret' => env('ESI_SECRET'),
        'redirect' => env('ESI_REDIRECT'),
        'scope' => env('ESI_SCOPE'),
        'user_agent' => env('ESI_USER_AGENT'),
        'base_url' => env('ESI_BASE_URL', 'https://esi.evetech.net/latest'),
        'category_skills' => 16,
        'test_character' => [
            'character_id' => env('ESI_TEST_CHARACTER_ID'),
            'refresh_token' => env('ESI_TEST_REFRESH_TOKEN'),
            'access_token' => env('ESI_TEST_ACCESS_TOKEN'),
        ],
    ],

];
